#! /bin/sh

HOST_NUMBER=$(echo ${HOST} | sed 's/[-a-z0]//g')

if [[ -z "${VERIFY}" ]]; then
  VERIFY="NO_VERIFY"
else
  VERIFY="VERIFY"
fi
"${CC:-gcc}" -O3 -D "HOST_NUMBER=${HOST_NUMBER}" "-D${VERIFY}" -mrtm latency-multi.c -o latency-multi -lpthread || exit 1

tests="txn_once faa"
for s in $(seq 90 10 120); do
  tests="${tests} txn_once_spin_${s}"
done

# Allow hyperthreading
for t in 1 2 4 8 10 12 14 16 18 20 
do
  echo "Packed ${t}" >&2
  for name in ${tests}; do
    echo "${name}" >&2
    env PACKED= ./latency-multi ${t} ${name}
  done
done

for t in 1 2 4 8 10 12 14 16 18 20
do
  echo "Spread ${t}" >&2
  for name in ${tests}; do
    echo "${name}" >&2
    env SPREAD= ./latency-multi ${t} ${name}
  done
done

# Allow hyperthreading
for t in 1 2 4 8 16 20 24 28 32 36 40
do
  echo "NUMA Packed ${t}" >&2
  for name in ${tests}; do
    echo "${name}" >&2
    env PACKED= NUMA= ./latency-multi ${t} ${name}
  done
done

for t in 1 2 4 8 16 20 24 28 32 36 40
do
  echo "NUMA Spread ${t}" >&2
  for name in ${tests}; do
    echo "${name}" >&2
    env SPREAD= NUMA= ./latency-multi ${t} ${name}
  done
done


