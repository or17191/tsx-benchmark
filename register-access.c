#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include <sched.h>
#include <immintrin.h>


int main (int argc, char* argv[])
{
    register int foo = 0;
    int i;
    for(i = 0; i < 1000; ++i) {
      if(_xbegin() == _XBEGIN_STARTED) {
        foo++;
        _xabort(0xff);
      }
    }
    printf("%d\n", foo);
    return 0;
}
