#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include <sched.h>
#include <immintrin.h>

#if HOST_NUMBER == 3
#include "thread_pin.mad03.inl"
#elif HOST_NUMBER == 5
#include "thread_pin.mad05.inl"
#else
#error "Unknown host"
#endif

static __inline__ unsigned long long int read_tsc(void)
{
    unsigned upper, lower;
    __asm__ __volatile__ ("rdtsc" : "=a"(lower), "=d"(upper));
    return ((unsigned long long int)lower)|(((unsigned long long int)upper)<<32 );
}

float get_cpu_mhz(){
        static float cpu_mhz;
        FILE *pipe = popen( "grep \"cpu MHz\" /proc/cpuinfo | head -1 | cut -d: -f2 | sed -e \"s/ //g\"", "r" );

        if( pipe == NULL || fscanf(pipe, "%f", &cpu_mhz) != 1 ) {
                fprintf(stderr,"Cannot get 'cpu MHz' from /proc/cpuinfo\n");
                exit(1);
        }
        pclose(pipe);
        return(cpu_mhz);
}

char *get_cpu_name(){
        static char cpu_name[512];
        FILE *pipe = popen( "grep 'model name' /proc/cpuinfo | head -1 | cut -d: -f2 | sed -e 's/^ *//g'", "r" );

        if( pipe == NULL || fgets(cpu_name, sizeof(cpu_name)-1, pipe) == NULL ) {
                fprintf(stderr,"Cannot get 'model name' from /proc/cpuinfo\n");
                exit(1);
        }
        cpu_name[strlen(cpu_name)-1] = '\0';
        pclose(pipe);
        return(cpu_name);
}

#ifdef VERIFY
#define CONS_STORE() before_txn = counter
#define CONS_LOAD() if(before_txn == counter) { inconsistencies++; }
#else
#define CONS_STORE()
#define CONS_LOAD()
#endif // VERIFY

typedef unsigned long long uint64;
typedef unsigned int uint32;
typedef volatile uint64 counter_type __attribute__((aligned(64)));
const uint64 COUNT = 10LL * 1000LL * 1000LL;
volatile int buf[1024];
counter_type counter = 0;
counter_type seperate_counters[44];
uint64 thread_count = 0;

size_t get_spin_count() {
    int evaluated;
    if (getenv("NUMA")) {
        if (getenv("SPREAD"))
            evaluated = 21 * thread_count + 62;
        else
            evaluated = 7 * thread_count + 16;
    } else {
        if (getenv("SPREAD"))
            evaluated = 35 * thread_count - 47;
        else
            evaluated = 11 * thread_count - 30;
    }
    if (evaluated < 0) {
      return 0;
    } else {
      // Round to nearest 5.
      evaluated += 5 / 2;
      evaluated -= (evaluated % 5);
      return evaluated;
    }
}

#define DO_5(x) x; x; x; x; x
#define DO_10(x)	DO_5(x); DO_5(x)
#define DO_20(x)	DO_10(x); DO_10(x)
#define DO_50(x)	DO_20(x); DO_20(x); DO_10(x)
#define DO_100(x)	DO_50(x); DO_50(x)

static __inline__ int spin(size_t s)
{
  volatile int x = 0; 
  int i; 
  for(i = 0; i < s / 5; ++i) {
    DO_5(x = 0);
  }
  return x;
}

#define ERROR_CODES 0x40

struct result {
    double cycles;
    long ops;
    long fails;
    long fail_codes[ERROR_CODES];
    long inconsistencies;
    int id;
};

pthread_barrier_t barr;


#define BENCHMARK(NAME, BODY)\
void* run_##NAME (void* r) \
{ \
    register uint64 fails = 0, old = 0, prev = 0, value = 0; \
    struct result *res = r; \
    uint64 id = (uint64)res->id << 32; \
    int x = 0; \
 \
    thread_pin(res->id); \
    counter = 0; \
    pthread_barrier_wait(&barr); \
 \
    unsigned long long t0 = read_tsc(); \
    for (value = 0; value < COUNT/100; value++) \
    { \
        DO_100(BODY); \
    } \
    unsigned long long t1 = read_tsc(); \
 \
    res->cycles = t1-t0; \
    res->ops = COUNT; \
    res->fails = fails; \
 \
    return NULL; \
}

BENCHMARK(cas, for (old = counter; !__sync_bool_compare_and_swap(&counter, old, id | (old+1)); fails++, old = counter))
BENCHMARK(cas_once, old = counter; if (!__sync_bool_compare_and_swap(&counter, old, id | (old+1))) fails++;)
BENCHMARK(faa,__sync_fetch_and_add(&counter, 1);)
BENCHMARK(read_faa,old = counter; __sync_fetch_and_add(&counter, 1);)
BENCHMARK(read_write,old = counter; counter = old)
BENCHMARK(cas_once_no_read, old = 1; __sync_bool_compare_and_swap(&counter, old, 2); fails++;)


#define _XBEGIN_STARTED (~0u)
#define _XABORT_EXPLICIT (1 << 0)
#define _XABORT_RETRY (1 << 1)
#define _XABORT_CONFLICT (1 << 2)
#define _XABORT_CAPACITY (1 << 3)
#define _XABORT_DEBUG (1 << 4)
#define _XABORT_NESTED (1 << 5)

#define BENCHMARK_TXN(NAME,BODY)\
void* run_##NAME(void* r)\
{\
    register uint64 fails = 0, old = 0, value = 0;\
    register uint64 inconsistencies = 0, before_txn; \
    uint64 fail_codes[ERROR_CODES] = {0}; \
    struct result *res = r;\
    uint64 id = (uint64)res->id << 32;\
    int ret = 0, ret2 = 0;\
    int i;\
    counter_type spin_counter = 0;\
\
    thread_pin(res->id);\
    counter = 0;\
    pthread_barrier_wait(&barr);\
\
    unsigned long long t0 = read_tsc();\
    for (value = 0; value < COUNT/100; value++) \
    { \
        DO_100(BODY); \
    } \
    unsigned long long t1 = read_tsc();\
\
    res->cycles = t1-t0;\
    res->ops = COUNT;\
    res->fails = fails;\
    memcpy(res->fail_codes, fail_codes, sizeof(fail_codes));\
    res->inconsistencies = inconsistencies;\
\
    return NULL;\
}

BENCHMARK_TXN(txn,
    while (1) {
        if ((ret = _xbegin()) == _XBEGIN_STARTED) {
            old = counter;
            counter = id | (old + 1);
            _xend();
            break;
        } else {
            fails++;
        }
        if (ret == 0)
            counter = counter;
    }
)

BENCHMARK_TXN(txn_once,
    CONS_STORE();
    while (1) {
        if ((ret = _xbegin()) == _XBEGIN_STARTED) {
            old = counter;
            counter = id | (old + 1);
            _xend();
        } else {
           fails++;
        }
        if (ret >= 0 && ret < ERROR_CODES) {
          fail_codes[ret]++;
        }
        if((ret & _XABORT_CONFLICT) != 0) break; 
    }
    CONS_LOAD();
)

BENCHMARK_TXN(txn_once_read_only,
    CONS_STORE();
    while (1) {
        if ((ret = _xbegin()) == _XBEGIN_STARTED) {
            old = counter;
            _xend();
        } else {
           fails++;
        }
        if (ret >= 0 && ret < ERROR_CODES) {
          fail_codes[ret]++;
        }
        //if (ret == 0) { counter = counter; }
        break;
    }
    CONS_LOAD();
)

BENCHMARK_TXN(txn_once_nested,
    CONS_STORE();
    while (1) {
        if ((ret = _xbegin()) == _XBEGIN_STARTED) {
            old = counter;
            old = id | (old + 1);
            if ((ret2 = _xbegin()) == _XBEGIN_STARTED) {
              counter = old;
              _xend();
            }
            _xend();
        } else {
           fails++;
        }
        if (ret >= 0 && ret < ERROR_CODES) {
          fail_codes[ret]++;
        }
        //if (ret == 0) { counter = counter; }
        if(ret == _XBEGIN_STARTED ||
          ((ret & _XABORT_NESTED) == 0 && (ret & _XABORT_CONFLICT) != 0)) break; 
    }
    CONS_LOAD();
)


#define BENCHMARK_TXN_SPIN(COUNT) \
  BENCHMARK_TXN(txn_once_spin_##COUNT, \
      CONS_STORE(); \
      while (1) { \
          if ((ret = _xbegin()) == _XBEGIN_STARTED) { \
              old = counter; \
              for(i = 0; i < COUNT / 5; ++i) { \
                DO_5(spin_counter = spin_counter); \
              } \
              old += spin_counter; \
              counter = id | (old + 1); \
              _xend(); \
          } else { \
             fails++; \
          } \
          if (ret >= 0 && ret < ERROR_CODES) { \
            fail_codes[ret]++; \
          } \
          /*break;*/ \
          if((ret & _XABORT_CONFLICT) != 0) break; \
      } \
      _mm_lfence(); \
      CONS_LOAD(); \
  )

BENCHMARK_TXN_SPIN(30)
BENCHMARK_TXN_SPIN(35)
BENCHMARK_TXN_SPIN(40)
BENCHMARK_TXN_SPIN(45)
BENCHMARK_TXN_SPIN(50)
BENCHMARK_TXN_SPIN(55)
BENCHMARK_TXN_SPIN(60)
BENCHMARK_TXN_SPIN(65)
BENCHMARK_TXN_SPIN(70)
BENCHMARK_TXN_SPIN(80)
BENCHMARK_TXN_SPIN(90)
BENCHMARK_TXN_SPIN(100)
BENCHMARK_TXN_SPIN(110)
BENCHMARK_TXN_SPIN(120)

void* run_seperate_txn(void* r)
{
    register uint64 fails = 0, old = 0, value = 0;
    register uint64 inconsistencies = 0, before_txn;
    uint64 fail_codes[ERROR_CODES] = {0};
    size_t spin_count = get_spin_count();
    struct result *res = r;
    uint64 id = (uint64)res->id << 32;
    int ret = 0;
    counter_type* my_counter = &seperate_counters[res->id];

    thread_pin(res->id);
    *my_counter = 0;
    pthread_barrier_wait(&barr);

    unsigned long long t0 = read_tsc();
    for (value = 0; value < COUNT/100; value++)
    {
        DO_100(
            CONS_STORE();
            while (1) {
                if ((ret = _xbegin()) == _XBEGIN_STARTED) {
                    old = *my_counter;
                    *my_counter = id | (old + 1);
                    _xend();
                } else {
                   fails++;
                }
                if (ret >= 0 && ret < ERROR_CODES) {
                  fail_codes[ret]++;
                }
                if (ret == 0) { counter = counter; }
                else if((ret & _XABORT_CONFLICT) != 0) break; 
            }
            CONS_LOAD();
        );
    }
    unsigned long long t1 = read_tsc();

    res->cycles = t1-t0;
    res->ops = COUNT;
    res->fails = fails;
    res->inconsistencies = inconsistencies;
    memcpy(res->fail_codes, fail_codes, sizeof(fail_codes));

    return NULL;
}

#define BENCHMARK_TXN_ADAPTIVE(NAME, BODY) \
void* run_txn_adaptive_##NAME(void* r) \
{ \
    register uint64 fails = 0, old = 0, value = 0; \
    register uint64 inconsistencies = 0, before_txn; \
    uint64 fail_codes[ERROR_CODES] = {0}; \
    size_t spin_count = get_spin_count(); \
    struct result *res = r; \
    uint64 id = (uint64)res->id << 32; \
    int ret = 0, ret2 = 0; \
 \
    thread_pin(res->id); \
    counter = 0; \
    pthread_barrier_wait(&barr); \
 \
    unsigned long long t0 = read_tsc(); \
    for (value = 0; value < COUNT/100; value++) \
    { \
        DO_100( \
            BODY \
        ); \
    } \
    unsigned long long t1 = read_tsc(); \
 \
    res->cycles = t1-t0; \
    res->ops = COUNT; \
    res->fails = fails; \
    res->inconsistencies = inconsistencies; \
    memcpy(res->fail_codes, fail_codes, sizeof(fail_codes)); \
 \
    return NULL; \
}

BENCHMARK_TXN_ADAPTIVE(simple,
    CONS_STORE();
    while (1) {
        if ((ret = _xbegin()) == _XBEGIN_STARTED) {
            old = counter;
            spin(spin_count);
            counter = id | (old + 1);
            _xend();
        } else {
           fails++;
        }
        if (ret >= 0 && ret < ERROR_CODES) {
          fail_codes[ret]++;
        }
        if (ret == 0) { counter = counter; }
        else if((ret & _XABORT_CONFLICT) != 0) break; 
    }
    CONS_LOAD();
)

BENCHMARK_TXN_ADAPTIVE(lfence,
    CONS_STORE();
    while (1) {
        if ((ret = _xbegin()) == _XBEGIN_STARTED) {
            old = counter;
            spin(spin_count);
            _mm_lfence();
            counter = id | (old + 1);
            _xend();
        } else {
           fails++;
        }
        if (ret >= 0 && ret < ERROR_CODES) {
          fail_codes[ret]++;
        }
        if (ret == 0) { counter = counter; }
        else if((ret & _XABORT_CONFLICT) != 0) break; 
    }
    CONS_LOAD();
)


BENCHMARK_TXN_ADAPTIVE(dependent,
    CONS_STORE();
    while (1) {
        if ((ret = _xbegin()) == _XBEGIN_STARTED) {
            old = counter;
            old += spin(spin_count); \
            *(&counter + (old >> 63)) = id | (old + 1);
            _xend();
        } else {
           fails++;
        }
        if (ret >= 0 && ret < ERROR_CODES) {
          fail_codes[ret]++;
        }
        if((ret & _XABORT_CONFLICT) != 0) break; 
    }
    CONS_LOAD();
)

BENCHMARK_TXN_ADAPTIVE(dependent_nested,
    CONS_STORE();
    while (1) {
        if ((ret = _xbegin()) == _XBEGIN_STARTED) {
            old = counter;
            old += spin(spin_count);
            if ((ret2 = _xbegin()) == _XBEGIN_STARTED) {
              *(&counter + (old >> 63)) = id | (old + 1);
              _xend();
            }
            _xend();
        } else {
           fails++;
        }
        if (ret >= 0 && ret < ERROR_CODES) {
          fail_codes[ret]++;
        }
        if((ret & _XABORT_CONFLICT) != 0) break; 
    }
    CONS_LOAD();
)

BENCHMARK_TXN_ADAPTIVE(test,
    CONS_STORE();
    while (1) {
        if ((ret = _xbegin()) == _XBEGIN_STARTED) {
            old = counter;
            spin(spin_count);
            if (counter == old) {
              counter = id | (old + 1);
            }
            _xend();
        } else {
           fails++;
        }
        if (ret >= 0 && ret < ERROR_CODES) {
          fail_codes[ret]++;
        }
        if((ret & _XABORT_CONFLICT) != 0) break; 
    }
    CONS_LOAD();
)

typedef struct test {
    char *name;
    void* (*func)(void *);
} test_t;

#define TEST(name) { #name, run_##name }

test_t tests[] = {
//    TEST(cas),
//    TEST(txn),
//    TEST(cas_once),
    TEST(txn_once),
//    TEST(txn_once_read_only),
//    TEST(seperate_txn),
//    TEST(txn_once_nested),
//    TEST(txn_adaptive_simple),
//    TEST(txn_adaptive_dependent),
//    TEST(txn_adaptive_dependent_nested),
//    TEST(txn_adaptive_lfence),
//    TEST(txn_adaptive_dependent),
//    TEST(txn_adaptive_test),
//    TEST(txn_once_spin_30),
//    TEST(txn_once_spin_40),
//    TEST(txn_once_spin_50),
//    TEST(txn_once_spin_60),
//    TEST(txn_once_spin_70),
//    TEST(txn_once_spin_80),
    TEST(txn_once_spin_90),
    TEST(txn_once_spin_100),
    TEST(txn_once_spin_110),
    TEST(txn_once_spin_120),
    TEST(faa),
//    TEST(read_faa),
//    TEST(read_write),
//    TEST(cas_once_no_read),
    { NULL, NULL },
};

int main (int argc, char* argv[])
{
    const int NUM_THREADS = atoi(argv[1]);
    thread_count = NUM_THREADS;

    pthread_t threads[NUM_THREADS];
    struct result res[NUM_THREADS];
    void* status;
    long i;
    test_t *test;

    void print_results(char *op) {
        double cycles = 0.0;
        long ops = 0, fails = 0;
        long j, k;
        long fail_codes[ERROR_CODES] = {0};
        long inconsistencies = 0;

        for (j = 0; j < NUM_THREADS; j++) {
            cycles += res[j].cycles;
            ops += res[j].ops;
            fails += res[j].fails;
            for(k = 0; k < ERROR_CODES; ++k) {
              fail_codes[k] += res[j].fail_codes[k];
            }
            inconsistencies += res[j].inconsistencies;
        }

        printf("%s %d cycles=%.3f ns=%.3f fails=%.3f inconsistencies=%ld counter=%lld/%ld fail_codes=", op, NUM_THREADS, cycles/ops, (cycles/get_cpu_mhz())/ops, (double)fails/ops, inconsistencies, counter & 0xffffffff, ops);
        for (k = 0; k < ERROR_CODES; ++k) {
          printf("%ld,%.3f,", k, (double)fail_codes[k]/ops);
        }
        printf("\n");
    }
    printf("%s %s %s-socket\n", get_cpu_name(), getenv("SPREAD") ? "SPREAD" : "PACKED", getenv("NUMA") ? "dual" : "single");

    pthread_barrier_init(&barr, NULL, NUM_THREADS);

    for (test = tests; test->func; test++) {
        if (argc == 3 && strcmp(test->name, argv[2]) != 0) {
          continue;
        }

        for (i = 0; i < NUM_THREADS; i++) {
            memset(&res[i], 0, sizeof(res[i]));
            res[i].id = i;
            pthread_create(&threads[i], NULL, test->func, &res[i]);
        }
 
        for (i = 0; i < NUM_THREADS; i++) {
            pthread_join(threads[i], &status);
        }

        print_results(test->name);
    }

    return 0;
}
